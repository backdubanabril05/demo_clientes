FROM openjdk:8-jdk-alpine
COPY target/Demo_Clientes-0.0.1.jar Clientes.jar
EXPOSE 8083
ENTRYPOINT ["java", "-jar", "/Clientes.jar"]